package ru.rencredit.jschool.kuzyushin.tm.service;

import ru.rencredit.jschool.kuzyushin.tm.api.service.IAuthService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyLoginException;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyPasswordException;
import ru.rencredit.jschool.kuzyushin.tm.exception.user.AccessDeniedException;
import ru.rencredit.jschool.kuzyushin.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }
}
