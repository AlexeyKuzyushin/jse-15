package ru.rencredit.jschool.kuzyushin.tm.repository;

import ru.rencredit.jschool.kuzyushin.tm.api.repository.ICommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.authentication.LoginCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.authentication.LogoutCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.project.*;
import ru.rencredit.jschool.kuzyushin.tm.command.system.*;
import ru.rencredit.jschool.kuzyushin.tm.command.task.*;
import ru.rencredit.jschool.kuzyushin.tm.command.user.*;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        commandList.add(new LoginCommand());
        commandList.add(new LogoutCommand());

        commandList.add(new UserListCommand());
        commandList.add(new UserRegistryCommand());
        commandList.add(new UserRemoveByIdCommand());
        commandList.add(new UserUpdateEmailCommand());
        commandList.add(new UserUpdateFirstNameCommand());
        commandList.add(new UserUpdateLastNameCommand());
        commandList.add(new UserUpdateLoginCommand());
        commandList.add(new UserUpdateMiddleNameCommand());
        commandList.add(new UserUpdatePasswordCommand());
        commandList.add(new UserViewProfileCommand());


        commandList.add(new ProjectClearCommand());
        commandList.add(new ProjectCreateCommand());
        commandList.add(new ProjectListCommand());
        commandList.add(new ProjectRemoveByIdCommand());
        commandList.add(new ProjectRemoveByIndexCommand());
        commandList.add(new ProjectRemoveByNameCommand());
        commandList.add(new ProjectUpdateByIdCommand());
        commandList.add(new ProjectUpdateByIndexCommand());
        commandList.add(new ProjectViewByIdCommand());
        commandList.add(new ProjectViewByIndexCommand());
        commandList.add(new ProjectViewByNameCommand());

        commandList.add(new TaskClearCommand());
        commandList.add(new TaskCreateCommand());
        commandList.add(new TaskListCommand());
        commandList.add(new TaskRemoveByIdCommand());
        commandList.add(new TaskRemoveByIndexCommand());
        commandList.add(new TaskRemoveByNameCommand());
        commandList.add(new TaskUpdateByIdCommand());
        commandList.add(new TaskUpdateByIndexCommand());
        commandList.add(new TaskViewByIdCommand());
        commandList.add(new TaskViewByIndexCommand());
        commandList.add(new TaskViewByNameCommand());

        commandList.add(new AboutCommand());
        commandList.add(new ExitCommand());
        commandList.add(new HelpCommand());
        commandList.add(new ShowArgCommand());
        commandList.add(new ShowCmdCommand());
        commandList.add(new SystemInfoCommand());
        commandList.add(new VersionCommand());
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }
}
