package ru.rencredit.jschool.kuzyushin.tm.command.user;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateLastNameCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-update-last-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user last name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        final User userUpdated = serviceLocator.getUserService().updateLastName(id, lastName);
        if (userUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }
}
