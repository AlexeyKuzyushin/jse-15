package ru.rencredit.jschool.kuzyushin.tm.command;

import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void execute();
}
