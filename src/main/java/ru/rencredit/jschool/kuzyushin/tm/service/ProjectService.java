package ru.rencredit.jschool.kuzyushin.tm.service;

import ru.rencredit.jschool.kuzyushin.tm.api.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyIdException;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyUserIdException;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectIndexException;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyNameException;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void add(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Override
    public List<Project> findALl(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAll(userId);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

    @Override
    public void create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(userId, id);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeById(userId, id);
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeByIndex(userId, index);
    }

    @Override
    public Project updateOneById(
            final String userId, final String id,
            final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateOneByIndex(
            final String userId, final Integer index,
            final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }
}
