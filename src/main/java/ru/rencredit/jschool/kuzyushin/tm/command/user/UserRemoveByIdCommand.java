package ru.rencredit.jschool.kuzyushin.tm.command.user;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserRemoveByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove user by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().removeById(id);
        if (user == null) {
            System.out.println("[FAILED]");
        } else {
            System.out.println("[OK]");
        }
    }
}
